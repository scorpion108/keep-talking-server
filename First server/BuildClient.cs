﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace First_server
{
    class BuildClient
    {
        #region Veriables
        DataBase databaseObject = new DataBase();
        //string fullName;
        string nickName;
        int password;
        BuildClient bc;
        #endregion

        public BuildClient(string nickname, int password)
        {
            //this.fullName = fullName;
            this.nickName = nickname;
            this.password = password;
        }

        public int GetID(string user)
        {
            int index = 0;
            string Quary = "SELECT * FROM User";
            SQLiteCommand myCommand1 = new SQLiteCommand(Quary, databaseObject.myConnection);
            databaseObject.OpenConnection();
            SQLiteDataReader result2 = myCommand1.ExecuteReader();
            if (result2.HasRows)
            {
                while (result2.Read())
                {
                    if (nickName.Equals(result2["NickName"]))
                    {
                        index = int.Parse(result2["id"].ToString());
                    }
                }
            }
            return index; 
        }

        public void Build()
        {
            string quary = "INSERT INTO User ('NickName', 'password') VALUES (@NickName, @password) ";
            SQLiteCommand myCommand = new SQLiteCommand(quary, databaseObject.myConnection);
            inputIntoDatabase(myCommand);

        }

        private void inputIntoDatabase(SQLiteCommand myCommand)
        {
            databaseObject.OpenConnection();
            createUser(myCommand, nickName, password);

            try
            {
                var result = myCommand.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine("The nickname is already in use ");
            }

            databaseObject.CloseConnection();
        }

        //private void checkDuplicats(SQLiteCommand myCommand) // Checks if 
        //{
            
        //    string Quary = "SELECT * FROM User";
        //    SQLiteCommand myCommand1 = new SQLiteCommand(Quary, databaseObject.myConnection);
        //    databaseObject.OpenConnection();
        //    SQLiteDataReader result2 = myCommand1.ExecuteReader();
        //    if (result2.HasRows)
        //    {
        //        while (result2.Read())
        //        {
        //            if (nickName.Equals(result2["nickName"]))
        //            {
        //                MessageBox.Show("The nickname is in use ");
        //            }
        //        }
        //    }

        //}

        public bool checkIfExists(BuildClient bc ) // Checks if the username exists in the system or not
        {
            int index = -1;
            
            string Quary = "SELECT * FROM User";
            SQLiteCommand myCommand1 = new SQLiteCommand(Quary, databaseObject.myConnection);
            databaseObject.OpenConnection();
            SQLiteDataReader result2 = myCommand1.ExecuteReader();
            if (result2.HasRows)
            {
                while (result2.Read())
                {
                    if (bc.nickName.Equals(result2["NickName"]))
                    {
                        index = int.Parse(result2["id"].ToString());
                    }
                }
            }

            if (index == -1)
                return false;
            else
                return true; 
        }

        public bool checkIfTrue() // Checks if the password is for the username. 
        {
            int index = 1;
            int index1 = 0;
            string Quary = "SELECT * FROM User";
            SQLiteCommand myCommand1 = new SQLiteCommand(Quary, databaseObject.myConnection);
            databaseObject.OpenConnection();
            SQLiteDataReader result2 = myCommand1.ExecuteReader();
            if (result2.HasRows)
            {
                while (result2.Read())
                {
                    if(nickName.Equals(result2["NickName"]))
                    {
                        index = int.Parse(result2["id"].ToString());
                    }
                    if (password == int.Parse(result2["Password"].ToString()))
                    {
                        index1 = int.Parse(result2["id"].ToString());
                    }
                }
            }

            if (index == index1)
                return true;
            else
                return false;
        }




        private void createUser(SQLiteCommand myCommand, string NickName, int password)
        {
            myCommand.Parameters.AddWithValue("@NickName", NickName);
            myCommand.Parameters.AddWithValue("@password", password);
        }

    }
}

