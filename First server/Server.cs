﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Data.SQLite;

namespace First_server
{
    public partial class Server : Form
    {
        #region Veribles

        private string IpAddress;
        private TcpListener tcpLsn;
        private Thread tcpThd;
        ClientConnect c1;
        List<ClientConnect> allClients = new List<ClientConnect>();
        private static int connectId = 0; // saves the amount of people that has connected
        string NameOfUsers;
        BuildClient bc;
        DataBase databaseObject = new DataBase();

        #endregion

        public Server()
        {
            InitializeComponent();
        }




        private void GetIpAdDress() //Saves the ip of the current pc
        {
            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());
            IpAddress = Convert.ToString(localIP[localIP.Length - 1]);
            // IpAddress ="127.0.0.1";        
        }

        private void Server_Load(object sender, EventArgs e)
        {
            GetIpAdDress();
            tcpLsn = new TcpListener(IPAddress.Parse(IpAddress), 8002);// creating a listener 
            tcpLsn.Start();// activates the lisener
            lblInfo.Text = "Listen at: " + tcpLsn.LocalEndpoint.ToString();// shows the addres
            c1 = new ClientConnect();
            tcpThd = new Thread(new ThreadStart(NewClientConnected));// create a new thread   
            tcpThd.Start();

        }

        public void NewClientConnected()
        {
            Socket s;
            string strmess;
            bool flag = false;
            string[] temp = new string[30];

            while (true)
            {
                try
                {
                    s = tcpLsn.AcceptSocket();
                    c1 = new ClientConnect();
                    c1.clientSocket = s;
                    c1.clientThread = new Thread(new ThreadStart(ReadSocket)); //saves the value
                    int ret = 0;
                    Byte[] receive = new Byte[10];
                    ret = s.Receive(receive, receive.Length, 0);// holds the  amount of bytes 
                    strmess = Encoding.UTF8.GetString(receive);// converts the array that was in byes into string and saves it into the strmess 
                    temp = strmess.Substring(0, ret).Split('$');// gets all the strings until the first space

                    c1.name = temp[0];
                    c1.password = int.Parse(temp[1]);
                    bc = new BuildClient(c1.name, c1.password);
                    // Checks if came from Register form or came from the Login Screen
                    if (c1.name[0].Equals('#')) // '#' indicate that came from register form  
                    {
                        c1.name = c1.name.Substring(1);
                        bc = new BuildClient(c1.name, c1.password);
                        bc.Build();
                    }

                    else if (!bc.checkIfTrue()) // checks if the user input the right password and username
                    {
                        //MessageBox.Show("Loser");

                        flag = true;
                    }
                    //else
                    //{
                    //    flag = true;
                    //    Send(flag);
                    //}


                    Interlocked.Increment(ref connectId);
                    c1.clientnum = connectId;
                    UpDateDataGrid(connectId + " : " + c1.name + "\n");
                    lock (this)
                    {
                        allClients.Add(c1);// updates the table and adds the hour that the client has connected מעדכנות את הטבלה ומוסיפות לתקסטבוקס את שעת 
                        UpDateDataGrid(" Connected > " + connectId + " " + DateTime.Now.ToLongTimeString());//
                        c1.clientThread.Start();
                    }

                    //if (flag)
                    //{
                    //    Send();
                    //    if (c1.clientSocket.Connected) c1.clientSocket.Close();
                    //    if (c1.clientThread.IsAlive) c1.clientThread.Abort();
                    //    UpDateDataGrid("Disconnected>" + c1.clientnum + " " + DateTime.Now.ToLongTimeString());

                    //    NewClientConnected();
                    //}

                    // builds the list of the participants
                    NameOfUsers = "@ ";
                    foreach (ClientConnect c in allClients)
                    {
                        if (c.clientSocket.Connected)
                        {
                            NameOfUsers += c1.clientnum + c.name + " ";
                        }
                    }




                    // we will convert all the clients into byes and send them 
                    Byte[] writeBuffer = new Byte[100];
                    var encord = new UTF8Encoding();// support hebrew 
                    writeBuffer = encord.GetBytes(NameOfUsers); // converts the names of the clients into bytes
                    foreach (ClientConnect c in allClients)
                    {
                        if (c.clientSocket.Connected)
                            c.clientSocket.Send(writeBuffer, writeBuffer.Length, SocketFlags.None);
                    }


                }
                catch (Exception e)
                {

                    break;
                }
            }
        }

        private void Send()
        {
            Byte[] writeBuffer = new Byte[1];
            var encord = new UTF8Encoding();// support hebrew 
            writeBuffer = encord.GetBytes("%"); // converts the names of the clients into bytes
            if (c1.clientSocket.Connected)
                c1.clientSocket.Send(writeBuffer, writeBuffer.Length, SocketFlags.None);
        }



        public void UpDateDataGrid(string displayString)//if we need to update the text box or the butttons, the form will perform an invoke 
        {
            if (txtData.InvokeRequired)
                txtData.Invoke(new MethodInvoker(() => txtData.AppendText(displayString + "\n")));

        }

        public void ReadSocket()// progress if each client
        {

            long realId = c1.clientnum; // The realId saves the real number of the client that sends the info   sender 
            Socket s = c1.clientSocket;
            int ret = 0; // This object will contain the number of characters that are passed in the message
            Byte[] receive; // In this array I'll save the info from the client
            receive = new Byte[2000]; // If the client is connected we reboot the array;
            while (true) // מנהל המשחק: כרגע מקבלת ממל מי כול לקוח ומוסרת אותו לכול הלקוחות
            {
                try
                {
                    if (s.Connected)
                    {
                        ret = s.Receive(receive, receive.Length, 0);//s.Receive is a command that gets the info from the client and put it in the array receive
                        if (ret > 0) // If a message is rececived
                        {
                            foreach (ClientConnect c in allClients)
                            {
                                if(GetReciever(ret , receive).Equals(c.name))
                                {
                                    if (c.clientSocket.Connected)
                                        c.clientSocket.Send(receive, receive.Length, SocketFlags.None);
                                    break; 
                                }  
                            }
                        }
                        else
                        {
                            break;
                        }
                    }// If a message was received and its' characters length is 0 we get out of the loop
                }
                catch (Exception e) // If an error occured we want to stop the thread
                {
                    UpDateDataGrid(e.ToString()); // Show on the screen in the txtbox the error that occured
                    if (!s.Connected) break;// If the client is not connected we get out of the loop
                }
            }

            CloseTheThread(realId);// We'll get to this line only if an error occured, because we only get out of the loop if there was an error
                                   //that's why this function will only be summaned if an error occured
        }
         
        private string GetReciever(int ret, Byte[] messege) // gets the first substring which contains the name of the reciver. 
        {
            string reciver;
            string strmess;
           
            strmess = Encoding.UTF8.GetString(messege);
            reciver = strmess.Substring(0, ret).Split('#')[0]; 

            return reciver;
        }

        private void CloseTheThread(long realId)
        {
            //This function closes the thread - the process of the client that caused the error
            try
            {
                for (int i = 0; i < allClients.Count; i++)
                {
                    if (allClients[i].clientnum == realId)
                        allClients[i].clientThread.Abort();
                }
            }

            catch (Exception e)
            {
                lock (this)
                {
                    for (int i = 0; i < allClients.Count; i++)
                    {
                        if (allClients[i].clientnum == realId)
                            allClients.Remove(allClients[i]);
                    }

                    UpDateDataGrid("Disconnected>" + realId + " " + DateTime.Now.ToLongTimeString());
                }
            }
        }

        private void OnClosing()
        {
            if (tcpLsn != null)
            { tcpLsn.Stop(); }

            foreach (ClientConnect cd in allClients)
            {
                if (cd.clientSocket.Connected) cd.clientSocket.Close();
                if (cd.clientThread.IsAlive) cd.clientThread.Abort();
            }

             
            if (tcpThd.IsAlive) tcpThd.Abort();

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            OnClosing();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            OnClosing();
        }
    }
}



