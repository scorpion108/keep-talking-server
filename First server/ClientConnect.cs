﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;

namespace First_server
{
    class ClientConnect
    {
        public int clientnum, password; // ID of the client that connected   //   Password of the client that connected
        public string name; // name - NickName of the client that connected  // fullName - the full name of the client
        public Socket clientSocket;
        public Thread clientThread;
    }
}
